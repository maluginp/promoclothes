package ru.malpen.promoclothes;

/**
 * Created by platon on 10.08.14.
 */
public class NumberAdder {
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void addNumber(int number) {
        this.number += number;
    }
}
