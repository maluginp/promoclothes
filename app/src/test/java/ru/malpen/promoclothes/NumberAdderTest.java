package ru.malpen.promoclothes;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by platon on 10.08.14.
 */
@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class NumberAdderTest {
    private NumberAdder numberAdder;
    @Before
    public void setUp() throws Exception {
        numberAdder = new NumberAdder();
    }

    @Test
    public void testNumber() throws Exception {
        numberAdder.setNumber(1);
        numberAdder.addNumber(3);
        Assert.assertEquals(numberAdder.getNumber(),4);
    }

    @Test
    public void testNumber1() throws Exception {
        numberAdder.setNumber(1);
        numberAdder.addNumber(3);
        Assert.assertEquals(numberAdder.getNumber(),5);
    }
}
